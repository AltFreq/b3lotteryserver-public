# coding=utf-8
import os
import socket
import sys

import pymysql
from slickrpc import Proxy

default_ticket_cost = 10

rpc_address = None
rpc_port = None
rpc_user = None
rpc_pass = None

with open(os.path.expanduser('~/.B3-CoinV2/b3coin.conf')) as file:
    line = file.readline()

    while line:
        setting = line.strip()
        if setting.startswith("rpcbind"):
            rpc_address = setting.split('=')[1]
        elif setting.startswith("rpcport"):
            rpc_port = setting.split('=')[1]
        elif setting.startswith("rpcpassword"):
            rpc_pass = setting.split('=', 1)[1]
        elif setting.startswith("rpcuser"):
            rpc_user = setting.split('=', 1)[1]
        line = file.readline()
b3coind = Proxy("http://%s:%s@%s:%s" % (rpc_user, rpc_pass, rpc_address, rpc_port))

print(sys.argv)
result = b3coind.gettransaction(sys.argv[1])
mysqql_ssl = {'cert': '/usr/lib/b3lotteryserver/client-cert.pem',
              'key': '/usr/lib/b3lotteryserver/client-key.pem'}
connection = None

tx = result['txid']
print(result)
details = result['details']
rxaddress = details[0]['address']
rxamount = result['amount']
print(rxamount)
if rxamount == 0:
    for outs in result['vout']:
        if outs['scriptPubKey']['type'] == 'pubkeyhash':
            rxamount = outs['value']
confirmations = result['confirmations']
tx_type = details[0]['category']


def establish_mysql():
    global connection
    connection = pymysql.connect(host='mysql_server',
                                 user='AltFreq',
                                 passwd='Ms016356!01',
                                 db="b3_lottery",
                                 charset="utf8",
                                 ssl=mysqql_ssl)


def get_mysql_query(string):
    if connection is None or not connection.open:
        establish_mysql()
    with connection as conn:
        sql_query = string
        conn.execute(sql_query)
        return conn.fetchall()


def get_mysql_query_args(string, *args):
    if connection is None or not connection.open:
        establish_mysql()
    with connection as conn:
        sql_query = string
        conn.execute(sql_query, args)
        return conn.fetchall()


def commit_mysql_query(string, *args):
    if connection is None or not connection.open:
        establish_mysql()
    with connection as conn:
        sql_query = string
        conn.execute(sql_query, args)
        connection.commit()


def round_down(num, divisor):
    return num - (num % divisor)


def post_message(message):
    s = socket.socket()
    s.connect(('127.0.0.1', 9000))
    s.sendall(message.encode())
    print("Message sent")
    s.close()


def post_tickets(amount):
    tickets = int(round_down(amount, default_ticket_cost) / 10)
    print(str(tickets) + " received")
    post_message("add_player " + str(tickets))


def post_node_payment(amount):
    print("Sending node payment message")
    post_message("node_full_payment " + str(amount))


print("Type = " + tx_type)
print('Receiving address = ' + rxaddress)
print('Received amount = ' + str(rxamount))
print('Confirmations = ' + str(confirmations))
node = False
sql = "SELECT * FROM nodes WHERE Node_Wallet = %s"
print(sql)
res = get_mysql_query_args(sql, rxaddress)
if len(res) > 0:
    node = True
    print("Is node address")
if node and (tx_type == 'immature' or tx_type == 'generate'):
    if rxamount > 5.0:
        sql = "INSERT INTO node_rewards (Node_ID, Transaction, Amount, Paid, Date) VALUES ((SELECT Node_Address FROM nodes WHERE Node_Wallet = %s), %s, %s, 0, UTC_DATE)"
        commit_mysql_query(sql, rxaddress, tx, rxamount)
        print("----Node deposit confirmation detected ----")
        print(tx)
        print('Amount: ' + str(rxamount))
        post_node_payment(tx)

print('--------------------------------------------\n   ')
