import asyncio
import os
import random
import ssl
import string

import pymysql
from slickrpc import Proxy

rpc_address = None
rpc_port = None
rpc_user = None
rpc_pass = None

with open(os.path.expanduser('~/.B3-CoinV2/b3coin.conf')) as file:
    line = file.readline()

    while line:
        setting = line.strip()
        if setting.startswith("rpcbind"):
            rpc_address = setting.split('=')[1]
        elif setting.startswith("rpcport"):
            rpc_port = setting.split('=')[1]
        elif setting.startswith("rpcpassword"):
            rpc_pass = setting.split('=', 1)[1]
        elif setting.startswith("rpcuser"):
            rpc_user = setting.split('=', 1)[1]
        line = file.readline()
b3coind = Proxy("http://%s:%s@%s:%s" % (rpc_user, rpc_pass, rpc_address, rpc_port))

mysqql_ssl = {'cert': 'client-cert.pem', 'key': 'client-key.pem'}
connection = None


def establish_mysql():
    global connection
    connection = pymysql.connect(host='mysql_server',
                                 user='AltFreq',
                                 passwd='Ms016356!01',
                                 db="b3_lottery",
                                 charset="utf8",
                                 ssl=mysqql_ssl)


def commit_mysql_query(string, *args):
    if connection is None or not connection.open:
        establish_mysql()
    try:
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql, args)
        connection.commit()
    except BrokenPipeError:
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql, args)
        connection.commit()
    finally:
        connection.close()

def get_mysql_query_arg(string, *args):
    if connection is None or not connection.open:
        establish_mysql()
    try:
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql, args)
            result = conn.fetchall()
    except BrokenPipeError:
        establish_mysql()
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql, args)
            result = conn.fetchall()
    finally:
        connection.close()
        return result


def get_link():
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.ascii_lowercase) for _ in range(6))


def get_generated_wallet(b3_wallet_link):
    print("Generating wallet")
    b3_wallet_link = str(b3_wallet_link)
    wallet_ref_used = b3_wallet_link.split(' ')
    print("Split " + str(wallet_ref_used))
    if len(wallet_ref_used) > 2:
        b3_wallet = wallet_ref_used[0]
        node_gen = b3coind.getnewaddress()
        print('New address '+ str(node_gen))
        sql = "INSERT INTO node_deposit_address (b3_wallet, Generated_Wallet) VALUES (%s, %s)"
        commit_mysql_query(sql, b3_wallet, node_gen)
        return node_gen
    else:
        print("else")
        sql = "INSERT INTO referrals (Link, Wallet_Address) VALUES (%s, %s) ON DUPLICATE KEY UPDATE Link=Link"
        print(sql)
        print(wallet_ref_used[1])
        print(wallet_ref_used[0])
        commit_mysql_query(sql, wallet_ref_used[1], wallet_ref_used[0])
        sql = "SELECT generated_wallet, Link, auto_play FROM wallet_addresses WHERE b3_wallet=%s"
        wallet_details = get_mysql_query_arg(sql, wallet_ref_used[0])
        if len(wallet_details) == 0:
            gen_wal = b3coind.getnewaddress()
            link = get_link()
            sql = "SELECT link FROM wallet_addresses WHERE link=%s"
            check_link = False
            while not check_link:
                res = get_mysql_query_arg(sql, link)
                if len(res) == 0:
                    check_link = True
                else:
                    link = get_link()
            sql = "INSERT INTO wallet_addresses (b3_wallet, generated_wallet, link, auto_play) VALUES (%s, %s, %s, %s)"
            print(sql)
            print(wallet_ref_used[0])
            print(gen_wal)
            print(link)
            commit_mysql_query(sql, wallet_ref_used[0], gen_wal, link, False)
        else:
            sql = "SELECT Generated_Wallet FROM node_deposit_address WHERE b3_wallet = %s"
            generated_node = get_mysql_query_arg(sql, wallet_ref_used[0])
            gen_wal = wallet_details[0][0]
            link = wallet_details[0][1]
            auto = wallet_details[0][2]
            if len(generated_node) > 0:
                gen_node = generated_node[0][0]
                return gen_wal + ' ' + link + ' ' + gen_node + ' ' + str(auto)
        return gen_wal + ' ' + link + ' ' + '0'



@asyncio.coroutine
def handle_echo(reader, writer):
    data = yield from reader.read(100)
    message = data.decode()

    addr = writer.get_extra_info('peername')
    print("Received "+str(message) + " from " + str(addr))
    generated_wallet = get_generated_wallet(message)

    print("Send: %r" % generated_wallet)
    writer.write(generated_wallet.encode())
    yield from writer.drain()

    print("Close the client socket")
    writer.close()

loop = asyncio.get_event_loop()
sslctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
sslctx.load_cert_chain(certfile='cert',
                       keyfile='key')
# coro = asyncio.start_server(handle_connection, '10.142.0.2', 9002, ssl=sslctx, loop=loop)
coro = asyncio.start_server(handle_echo, 'main_wallet', 9002, loop=loop)
server = loop.run_until_complete(coro)

# Serve requests until Ctrl+C is pressed
print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

# Close the server
server.close()
loop.run_until_complete(server.wait_closed())
loop.close()
