import socket
import ssl

sslctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
sslctx.load_cert_chain(certfile='cert',
                       keyfile='key')
# async def send_message(message, loop):
#     reader, writer = await asyncio.open_connection(
#     'localhost', 9000, ssl=sslctx, loop=loop)
#     writer.write(message.encode())
#     await writer.drain()
#     # data = await reader.readline()
#     # print("received =" + data.decode())
#     # assert data == b'pong\n', repr(data)
#     # print("Client received {!r} from server".format(data))
#     writer.close()
#     print('Client done')
#
#
# loop = asyncio.get_event_loop()
# loop.run_until_complete(send_message("remove_time 1800", loop))
# loop.close()
s = socket.socket()
# sslSocket = ssl.wrap_socket(s, certfile='cert', keyfile='key')
s.connect(('127.0.0.1', 9000))
message = "force_timer 20"
s.sendall(message.encode())
# result = sslSocket.recv()
# print(result.decode())
s.close()
