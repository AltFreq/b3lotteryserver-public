import os
import sys

from slickrpc import Proxy

rpc_address = None
rpc_port = None
rpc_user = None
rpc_pass = None

with open(os.path.expanduser('~/.B3-CoinV2/b3coin.conf')) as file:
    line = file.readline()
    while line:
        setting = line.strip()
        if setting.startswith("rpcbind"):
            rpc_address = setting.split('=')[1]
        elif setting.startswith("rpcport"):
            rpc_port = setting.split('=')[1]
        elif setting.startswith("rpcpassword"):
            rpc_pass = setting.split('=', 1)[1]
        elif setting.startswith("rpcuser"):
            rpc_user = setting.split('=', 1)[1]
        line = file.readline()

b3coind = Proxy("http://%s:%s@%s:%s" % (rpc_user, rpc_pass, rpc_address, rpc_port))

with open(os.path.expanduser(sys.argv[1])) as file:
    line = file.readline()
    while line:
        privkey = line
        print('importing ' + line)
        b3coind.importprivkey(privkey, "", False)
        line = file.readline()
