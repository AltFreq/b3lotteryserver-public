import os
from time import sleep

import pymysql
from slickrpc import Proxy

rpc_address = None
rpc_port = None
rpc_user = None
rpc_pass = None

with open(os.path.expanduser('~/.B3-CoinV2/b3coin.conf')) as file:
    line = file.readline()

    while line:
        setting = line.strip()
        if setting.startswith("rpcbind"):
            rpc_address = setting.split('=')[1]
        elif setting.startswith("rpcport"):
            rpc_port = setting.split('=')[1]
        elif setting.startswith("rpcpassword"):
            rpc_pass = setting.split('=', 1)[1]
        elif setting.startswith("rpcuser"):
            rpc_user = setting.split('=', 1)[1]
        line = file.readline()
b3coind = Proxy("http://%s:%s@%s:%s" % (rpc_user, rpc_pass, rpc_address, rpc_port))

mysqql_ssl = {'cert': '/usr/lib/b3lotteryserver/client-cert.pem',
              'key': '/usr/lib/b3lotteryserver/client-key.pem'}
connection = None


def establish_mysql():
    global connection
    connection = pymysql.connect(host='mysql_server',
                                 user='AltFreq',
                                 passwd='Ms016356!01',
                                 db="b3_lottery",
                                 charset="utf8",
                                 ssl=mysqql_ssl)


def get_mysql_query_arg(string, *args):
    if connection is None or not connection.open:
        establish_mysql()
    with connection as conn:
        sql = string
        conn.execute(sql, args)
        conn.close()
        return conn.fetchall()


def commit_mysql_query(string, *args):
    if connection is None or not connection.open:
        establish_mysql()
    with connection as conn:
        sql_query = string
        conn.execute(sql_query, args)
        connection.commit()
        conn.close()


def strip_data(data):
    data = data.strip()
    data = data.replace(':5647', '')
    data = data.replace('[', '')
    data = data.replace(']', '')
    return data


def process_node(row):
    global nodes
    pose_scores = b3coind.fundamentalnodelist("pose", row[0])
    pose = None
    for address in pose_scores:
        pose = pose_scores[address]

    active_seconds = b3coind.fundamentalnodelist("activeseconds", row[0])
    active = None
    for address in pose_scores:
        active = active_seconds[address]

    node_status = b3coind.fundamentalnodelist("status", row[0])
    status = None
    for address in pose_scores:
        status = node_status[address]

    if row[0] in nodes:
        string = row[0]+"--"
        changed = False
        node = nodes[row[0]]
        old_status = node[row[0]]["status"]
        old_pose = node[row[0]]["pos_score"]
        if old_status != status:
            changed = True
            string = string + "Status changed from " + str(old_status) + " to " + str(status) + " "
        if old_pose != pose:
            changed = True
            string = string + "Pose Score changed from " + str(old_pose) + " to " + str(pose) + " "
        if changed:
            node[row[0]]["status"] = status
            node[row[0]]["pos_score"] = pose
            print(string)
            print("------------------------------------------------------------------")
    else:
        node = {row[0]: {'wallet': row[1], 'id': row[2], 'pos_score': pose, 'active': active, 'status': status}}
        nodes[row[0]] = node
        print("New node added: " + str(node))

nodes = {}

while True:
    sql = "SELECT Node_Address, Node_Wallet, ID FROM nodes"
    res = get_mysql_query_arg(sql)
    for row in res:
        process_node(row)


    sleep(200)
