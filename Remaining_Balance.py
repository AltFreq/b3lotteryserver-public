import os
from time import sleep

import pymysql
from slickrpc import Proxy

rpc_address = None
rpc_port = None
rpc_user = None
rpc_pass = None

with open(os.path.expanduser('~/.B3-CoinV2/b3coin.conf')) as file:
    line = file.readline()

    while line:
        setting = line.strip()
        if setting.startswith("rpcbind"):
            rpc_address = setting.split('=')[1]
        elif setting.startswith("rpcport"):
            rpc_port = setting.split('=')[1]
        elif setting.startswith("rpcpassword"):
            rpc_pass = setting.split('=', 1)[1]
        elif setting.startswith("rpcuser"):
            rpc_user = setting.split('=', 1)[1]
        line = file.readline()
b3coind = Proxy("http://%s:%s@%s:%s" % (rpc_user, rpc_pass, rpc_address, rpc_port))

mysqql_ssl = {'cert': '/usr/lib/b3lotteryserver/client-cert.pem',
              'key': '/usr/lib/b3lotteryserver/client-key.pem'}
connection = None


def establish_mysql():
    global connection
    connection = pymysql.connect(host='mysql_server',
                                 user='AltFreq',
                                 passwd='Ms016356!01',
                                 db="b3_lottery",
                                 charset="utf8",
                                 ssl=mysqql_ssl)


def get_mysql_query_arg(string, *args):
    if connection is None or not connection.open:
        establish_mysql()
    with connection as conn:
        sql = string
        conn.execute(sql, args)
        conn.close()
        return conn.fetchall()


def commit_mysql_query(string, *args):
    if connection is None or not connection.open:
        establish_mysql()
    with connection as conn:
        sql_query = string
        conn.execute(sql_query, args)
        connection.commit()
        conn.close()


balance = b3coind.getbalance()
node_total = 0
giveaway_total = 0
round_total = 0

sql = "select SUM(amount)+SUM(Direct_Deposit)+IFNULL((SELECT SUM(Amount) FROM node_deposit_pending),0)-(SELECT SUM(Cost) FROM nodes) from node_owners"
res = get_mysql_query_arg(sql)
for row in res:
    node_total = float(row[0])

sql = "select sum(Deposit_Amount) from completed_deposits group by Round_ID ORDER BY Round_ID  DESC limit 1"
res = get_mysql_query_arg(sql)
for row in res:
    round_total = float(row[0])

sql = "select amount from daily_giveaway ORDER BY ID DESC LIMIT 1"
res = get_mysql_query_arg(sql)
for row in res:
    giveaway_total = float(row[0])

print("Balance =" + str(balance))
print("Node total =" + str(node_total))
print("Giveaway total =" + str(giveaway_total))
print("Round total = " + str(round_total))
available = balance - node_total - giveaway_total - round_total
print("Available balance = " + str(available))
