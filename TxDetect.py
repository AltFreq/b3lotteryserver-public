import math
import os
import socket
import sys

import pymysql
from slickrpc import Proxy

default_ticket_cost = 10
rpc_address = None
rpc_port = None
rpc_user = None
rpc_pass = None

with open(os.path.expanduser('~/.B3-CoinV2/b3coin.conf')) as file:
    line = file.readline()

    while line:
        setting = line.strip()
        if setting.startswith("rpcbind"):
            rpc_address = setting.split('=')[1]
        elif setting.startswith("rpcport"):
            rpc_port = setting.split('=')[1]
        elif setting.startswith("rpcpassword"):
            rpc_pass = setting.split('=', 1)[1]
        elif setting.startswith("rpcuser"):
            rpc_user = setting.split('=', 1)[1]
        line = file.readline()
b3coind = Proxy("http://%s:%s@%s:%s" % (rpc_user, rpc_pass, rpc_address, rpc_port))

result = b3coind.gettransaction(sys.argv[1])
mysqql_ssl = {'cert': '/usr/lib/b3lotteryserver/client-cert.pem',
              'key': '/usr/lib/b3lotteryserver/client-key.pem'}
connection = None

tx = result['txid']
details = result['details']
rxaddress = 'none'
rxamount = 0
confirmations = result['confirmations']
tx_type = 'send'


def establish_mysql():
    global connection
    connection = pymysql.connect(host='mysql_server',
                                 user='AltFreq',
                                 passwd='Ms016356!01',
                                 db="b3_lottery",
                                 charset="utf8",
                                 ssl=mysqql_ssl)


def get_mysql_query_arg(string, *args):
    if connection is None or not connection.open:
        establish_mysql()
    with connection as conn:
        sql = string
        conn.execute(sql, args)
        conn.close()
        return conn.fetchall()


def commit_mysql_query(string, *args):
    if connection is None or not connection.open:
        establish_mysql()
    with connection as conn:
        sql_query = string
        conn.execute(sql_query, args)
        connection.commit()
        conn.close()


def round_down(num, divisor):
    return num - (num % divisor)


def post_message(message):
    s = socket.socket()
    s.connect(('127.0.0.1', 9000))
    s.sendall(message.encode())
    print("Message sent")
    s.close()


def post_tickets(amount):
    tickets = int(round_down(amount, default_ticket_cost) / 10)
    print(str(tickets) + " received")
    post_message("add_player " + str(tickets))


print('Transaction ' + tx + ' updated')
for detail in result['details']:
    tx_type = detail['category']
    if tx_type == 'receive':
        rxaddress = detail['address']
        rxamount = detail['amount']
        break

print("Type = " + tx_type)
print('Receiving address = ' + rxaddress)
print('Received amount = ' + str(rxamount))
print('Confirmations = ' + str(confirmations))


if tx_type == "receive" and confirmations > 0:
    print("checking for direct deposit")
    sql = "SELECT b3_wallet FROM node_deposit_address WHERE Generated_Wallet= %s"
    res = get_mysql_query_arg(sql, rxaddress)
    if len(res) > 0:
        print("Direct deposit detected")
        for row in res:
            wallet = row[0]
            if rxamount > 0:
                print("Direct deposit of " + str(rxamount) + "too small refunding")
                rxamount -= 0.001
                print("Refunding " + str(rxamount) + " to " + wallet)
                transaction = b3coind.sendtoaddress(wallet, rxamount)
                print("Refund tx = " + transaction)
                sql = "INSERT INTO refunds (rxtx, rxamount, refunded_amount, refunded_address) VALUES (%s, %s, %s, %s)"
                commit_mysql_query(sql, tx, (rxamount + 0.001), rxamount, wallet)
                sql = "INSERT INTO payment_history (Wallet_Address, Amount, Transaction_ID) VALUES (%s, %s, %s)"
                commit_mysql_query(sql, wallet, rxamount, transaction)
        exit()
    if rxamount > 0.001:
        refund_amount = rxamount
        if refund_amount > 0.004:
            rxamount -= refund_amount
            refund_amount -= 0.001
            sql = "SELECT b3_wallet FROM wallet_addresses WHERE generated_wallet=%s"
            b3wallet = get_mysql_query_arg(sql, rxaddress)
            print(b3wallet)
            b3wallet = b3wallet[0][0]
            print("Refunding " + str(refund_amount) + " to " + b3wallet)
            transaction = b3coind.sendtoaddress(b3wallet, refund_amount)
            print("Refund tx = " + transaction)
            sql = "INSERT INTO refunds (rxtx, rxamount, refunded_amount, refunded_address) VALUES (%s, %s, %s, %s)"
            commit_mysql_query(sql, tx, (rxamount + (refund_amount / 0.95)), refund_amount, b3wallet)
            sql = "INSERT INTO payment_history (Wallet_Address, Amount, Transaction_ID) VALUES (%s, %s, %s)"
            commit_mysql_query(sql, b3wallet, refund_amount, transaction)



print('--------------------------------------------\n   ')
