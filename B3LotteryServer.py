# coding=utf-8
import asyncio
import copy
import math
import os
import random
from datetime import timedelta, datetime

import pymysql
from slickrpc import Proxy

rpc_address = None
rpc_port = None
rpc_user = None
rpc_pass = None
rpc_address_nodes = None
rpc_port_nodes = None
rpc_user_nodes = None
rpc_pass_nodes = None

with open(os.path.expanduser('~/.B3-CoinV2/b3coin.conf')) as file:
    line = file.readline()

    while line:
        data = line.strip()
        if data.startswith("rpcbind"):
            rpc_address = data.split('=')[1]
        elif data.startswith("rpcport"):
            rpc_port = data.split('=')[1]
        elif data.startswith("rpcpassword"):
            rpc_pass = data.split('=', 1)[1]
        elif data.startswith("rpcuser"):
            rpc_user = data.split('=', 1)[1]
        line = file.readline()
b3coind = Proxy("http://%s:%s@%s:%s" % (rpc_user, rpc_pass, rpc_address, rpc_port))
with open(os.path.expanduser('/home/wallet/.B3-CoinV2/b3coin.conf')) as file:
    line = file.readline()

    while line:
        data = line.strip()
        if data.startswith("rpcbind"):
            rpc_address_nodes = data.split('=')[1]
        elif data.startswith("rpcport"):
            rpc_port_nodes = data.split('=')[1]
        elif data.startswith("rpcpassword"):
            rpc_pass_nodes = data.split('=', 1)[1]
        elif data.startswith("rpcuser"):
            rpc_user_nodes = data.split('=', 1)[1]
        line = file.readline()
b3coindnodes = Proxy("http://%s:%s@%s:%s" % (rpc_user_nodes, rpc_pass_nodes, rpc_address_nodes, rpc_port_nodes))

print("Testing wallet connections b3coind" + str(b3coind.getbalance()))
print("Testing wallet connections b3coindnodes" + str(b3coindnodes.getbalance()))

default_timer = 21600
default_decrease = 1800
default_increase = 1800
default_win = 0.9
default_ticket_cost = 10
min_players = 10
players_decrease = 100
round_id = 0

current_timer = 0
current_timer_val = 0
current_end = 0
current_players = 0

block_completed = False
game_finishing = False

node_payment = []
pending_node_payments = []

auto_player_balance = {}

mysqql_ssl = {'cert': '/usr/lib/b3lotteryserver/client-cert.pem', 'key': '/usr/lib/b3lotteryserver/client-key.pem'}
connection = None


async def format_datetime(date_time):
    return date_time.__format__('%Y-%m-%d %H:%M:%S')


async def get_datetime():
    return datetime.utcnow()


async def establish_mysql():
    global connection
    connection = pymysql.connect(host='mysql_server',
                                 user='AltFreq',
                                 passwd='Ms016356!01',
                                 db="b3_lottery",
                                 charset="utf8",
                                 ssl=mysqql_ssl)


async def get_future_datetime(date_time, seconds):
    return date_time + timedelta(seconds=int(seconds))


async def get_player_list():
    global current_players
    player_list = []
    for i in range(current_players):
        player_list.append(i)
    print("List - " + str(player_list))
    return player_list


async def get_round():
    global round_id
    return round_id


async def get_random_list(ticket_list):
    print('Getting randomed list')
    results = []
    for i in range(len(ticket_list)):
        secure_random = random.SystemRandom()
        choice = secure_random.choice(ticket_list)
        results.append(choice)
        ticket_list.remove(choice)
    # print(results)
    return results


async def get_random_list_length(length, ticket_list):
    print('Getting randomed list')
    results = []
    for i in range(length):
        secure_random = random.SystemRandom()
        choice = secure_random.choice(ticket_list)
        results.append(choice)
        ticket_list.remove(choice)
    # print(results)
    return results


async def round_to_nearest(num, divisor):
    return num - (num % divisor)


async def get_player_tickets():
    print("Getting tickets")
    sql = "SELECT Wallet_Address, FLOOR(SUM(Deposit_Amount)/%s) FROM completed_deposits WHERE Round_ID = (SELECT MAX(ID) FROM game_round) GROUP BY Wallet_Address"
    results = await get_mysql_query_arg(sql, default_ticket_cost)
    tickets = []
    for tuple in results:
        wallet_bal = [tuple[0], tuple[1]]
        tickets.append(wallet_bal)
    return tickets


async def get_tickets_list(results):
    print("Getting tickets list")
    tickets_list = []
    for row in results:
        wallet_address = row[0]
        total = row[1]
        tickets = int(total)
        for i in range(int(tickets)):
            tickets_list.append(wallet_address)

            #    print(tickets_list)
    return tickets_list


async def get_group_sizes(random_list):
    size = len(random_list)
    group_1 = round(0.1 * size, 0)
    group_2 = round(0.1 * size, 0)
    group_3 = round(0.2 * size, 0)
    group_4 = round(0.25 * size, 0)
    group_5 = round(size * default_win) - group_1 - group_2 - group_3 - group_4
    groups = [group_1, group_2, group_3, group_4, group_5]
    print('Group sizes:')
    print('g1 ' + str(group_1) + ' g2 ' + str(group_2) + ' g3 ' + str(group_3) + ' g4 ' + str(
        group_4) + ' g5 ' + str(
        group_5))
    return groups


def round_down(value):
    decimals = 100000000
    val = value * decimals
    return round(math.floor(val) / decimals, 8)


async def get_payout_amounts(group_sizes, losers_pool):
    group_1_percent = 0.4 / group_sizes[0]
    group_2_percent = 0.25 / group_sizes[1]
    group_3_percent = 0.12 / group_sizes[2]
    group_4_percent = 0.1 / group_sizes[3]
    group_5_percent = 0.04 / group_sizes[4]

    group_1_payout = round_down(default_ticket_cost + (losers_pool * group_1_percent))
    group_2_payout = round_down(default_ticket_cost + (losers_pool * group_2_percent))
    group_3_payout = round_down(default_ticket_cost + (losers_pool * group_3_percent))
    group_4_payout = round_down(default_ticket_cost + (losers_pool * group_4_percent))
    group_5_payout = round_down(default_ticket_cost + (losers_pool * group_5_percent))

    group = [group_1_payout, group_2_payout, group_3_payout, group_4_payout, group_5_payout]
    return group


async def get_winners_pool(groups):
    sum_pool = 0
    for g in groups:
        sum_pool += g
    return sum_pool * default_ticket_cost


async def get_remaining_funds(total_pool):
    val = total_pool * 0.0087
    print('Remaining funds =' + str(val))
    return val


async def add_to_payouts(random_list, payouts, group_size, payout_amount):
    for g in range(int(group_size)):
        deposit_wallet = random_list[0]
        random_list.remove(deposit_wallet)
        if deposit_wallet in payouts:
            payouts[deposit_wallet] += round_down(payout_amount)
        else:
            payouts[deposit_wallet] = round_down(payout_amount)


async def calculate_payments(random_list, group_sizes, payout_amounts):
    payments = {}  # wallet : total
    for i in range(len(group_sizes)):
        await add_to_payouts(random_list, payments, group_sizes[i], payout_amounts[i])
    return payments


async def make_payments(payments, main=False):
    global auto_player_balance
    if main:
        print("Removing auto players from payment processing")
        auto_player_wins = {}
        auto_wallets = []
        print("auto players " + str(auto_player_balance))
        for player in auto_player_balance:
            if player in payments:
                auto_player_wins[player] = payments[player]
                auto_wallets.append(player)
                payments.pop(player)

    transactions = {}
    print("Making payments ")
    payments_to_make = dict(payments)
    wallets = []
    if len(payments_to_make) <= 20 and len(payments_to_make) > 0:
        transaction = (b3coind.sendmany("", payments_to_make))
        for wallet in payments_to_make:
            wallets.append(wallet)
        transactions[transaction] = wallets
    else:
        smaller_payments = {}
        while len(payments_to_make) >= 20:
            for i in range(20):
                val = payments_to_make.popitem()
                smaller_payments[val[0]] = val[1]
                wallets.append(val[0])
            transaction = (b3coind.sendmany("", smaller_payments))
            transactions[transaction] = wallets
            smaller_payments = {}
            wallets = []
        if len(payments_to_make) > 0:
            transaction = (b3coind.sendmany("", payments_to_make))
            for wallet in payments_to_make:
                wallets.append(wallet)
            transactions[transaction] = wallets
    if main:
        print("Adding autoplayers back to payments")
        for player in auto_player_wins:
            payments[player] = auto_player_wins[player]
        print("adding autoplayer tx into tx's " + str(auto_wallets))
        if (len(auto_wallets) > 0):
            transactions["Auto Play - " + str(await get_round())] = auto_wallets
    print("Transactions done")
    return transactions


async def make_node_payments(payments):
    transactions = {}
    print("Making payments " + str(len(payments)))
    payments_to_make = dict(payments)
    wallets = []
    if len(payments_to_make) <= 20:
        transaction = (b3coindnodes.sendmany("", payments_to_make))
        for wallet in payments_to_make:
            wallets.append(wallet)
        transactions[transaction] = wallets
    else:
        smaller_payments = {}
        while len(payments_to_make) >= 20:
            for i in range(20):
                val = payments_to_make.popitem()
                smaller_payments[val[0]] = val[1]
                wallets.append(val[0])
            print("Making payment")
            print("b3coind sendmany(''," + str(smaller_payments) + ")")
            transaction = None
            try:
                transaction = (b3coindnodes.sendmany("", smaller_payments))
            except:
                failedTX = open("failedTx.txt", "a")
                string = 'b3coind sendmany "" \'{'
                for idx, payment in enumerate(range(len(smaller_payments))):
                    string += '"' + payment[0] + '": ' + payment[1]
                    if idx - 1 != len(smaller_payments):
                        string += ", "
                string += "}'"
                failedTX.write('b3coind sendmany "" '+string)
                failedTX.close()
            if transaction is not None:
                transactions[transaction] = wallets
            smaller_payments = {}
            wallets = []
            await asyncio.sleep(5)
        if len(payments_to_make) > 0:
            transaction = (b3coindnodes.sendmany("", payments_to_make))
            for wallet in payments_to_make:
                wallets.append(wallet)
            transactions[transaction] = wallets
    print("Transactions  done " + str(len(transactions)))
    return transactions


async def record_transaction(payments, transactions):
    tx_list = []
    for key in transactions:
        tx_list.append(key)
    sql = "UPDATE game_round SET Paid=%s, Paid_Transaction_ID=%s ORDER BY ID DESC LIMIT 1"
    await commit_mysql_query(sql, True, str(tx_list))
    for tx in transactions:
        for wallet in transactions[tx]:
            sql = "INSERT INTO payment_history (Wallet_Address, Amount,Transaction_ID) VALUES (%s, %s, %s)"
            await commit_mysql_query(sql, wallet, round_down(payments[wallet]), tx)


async def calculate_node_earnings(remaining_funds, random_list):
    print("Remaining funds = " + str(remaining_funds))
    # print("Random list = " + str(random_list))
    number_of_earners = len(random_list)
    print("number = " + str(number_of_earners))
    remaining = remaining_funds * 0.75
    print('Remaining after take = ' + str(remaining))
    amount = remaining / number_of_earners
    print('amount ' + str(amount))
    node_earnings = {}  # wallet:amount
    for wallet in random_list:
        if wallet in node_earnings:
            node_earnings[wallet] += round_down(amount)
        else:
            node_earnings[wallet] = round_down(amount)
    print("Node EArnings = " + str(node_earnings))
    return node_earnings


async def record_node_earnings(node_earnings):
    for wallet in node_earnings:
        sql = "INSERT INTO node_owners (Wallet_Address, Amount) VALUES (%s, %s) ON DUPLICATE KEY UPDATE Amount=Amount+%s"
        print("Wallet = " + str(wallet) + " node earnings = " + str(node_earnings[wallet]))
        await commit_mysql_query(sql, wallet, node_earnings[wallet], node_earnings[wallet])


async def find_wallet_tx(wallet, transactions):
    for tx in transactions:
        for wallets in transactions[tx]:
            if wallet == wallets:
                return tx


async def update_player_stats(tickets, payments, transactions, node_earnings):
    print("node earnings" + str(node_earnings))
    print("Updating player stats " + str(tickets))
    for wallet in tickets:
        wallet_address = wallet[0]
        ticket_count = wallet[1]
        win = 0
        if wallet_address in payments:
            win = payments[wallet_address]
        lose = 0
        if wallet_address in node_earnings:
            lose = node_earnings[wallet_address]
        sql = "INSERT INTO history_player (Round_ID, Wallet_Address, Tickets, Win_Total, Lose_Total, Paid_Transaction_ID) VALUES ((SELECT MAX(ID) FROM game_round), %s, %s, %s, %s, %s)"
        await commit_mysql_query(sql, wallet_address, ticket_count, round_down(win), round_down(lose),
                                 await find_wallet_tx(wallet_address, transactions))


async def update_giveaway(remaining):
    val = remaining * 0.25 * 0.85
    sql = 'UPDATE daily_giveaway SET Amount=Amount+%s ORDER BY start DESC LIMIT 1'
    await commit_mysql_query(sql, round_down(val))
    print("Giveaway added = " + str(val))


async def get_giveaway_tickets(start, end):
    print("Getting giveaway tickets " + str(start) + " " + str(end))
    tickets = []
    sql = 'SELECT b3_wallet, lottery_tickets+referral_tickets FROM giveaway_tickets WHERE giveaway_id = (SELECT MAX(ID) FROM daily_giveaway)'
    print('sql = ' + sql)
    res = await get_mysql_query_arg(sql)
    print('res = ' + str(res))
    for row in res:
        wallet = row[0]
        number = int(row[1])
        for i in range(number):
            tickets.append(wallet)
    return tickets


async def get_giveaway():
    giveaway = {}
    sql = "SELECT start, end, amount FROM daily_giveaway ORDER BY end DESC LIMIT 1"  # yesterday
    res = await get_mysql_query(sql)
    for row in res:
        giveaway['start'] = row[0]
        giveaway['end'] = row[1]
        giveaway['amount'] = row[2]
    return giveaway


async def get_giveaway_win_amounts(winners, amount):
    val = int(amount)
    print("Giveaway amount =" + str(amount))
    payouts = [{winners[0]: round(val * 0.42, 2)}, {winners[1]: round(val * 0.15, 2)},
               {winners[2]: round(val * 0.15, 2)}, {winners[3]: round(val * 0.08, 2)},
               {winners[4]: round(val * 0.08, 2)}, {winners[5]: round(val * 0.08, 2)},
               {winners[6]: round(val * 0.01, 2)}, {winners[7]: round(val * 0.01, 2)},
               {winners[8]: round(val * 0.01, 2)}, {winners[9]: round(val * 0.01, 2)}]
    return payouts


async def record_giveaway_transactions(transactions, payments, total_tickets):
    print('Recording transactions')
    for tx in transactions:
        print("Tx - " + tx)
        for wallet in transactions[tx]:
            print("wallet = " + wallet)
            for pay in payments:
                print("pay = " + str(pay))
                if wallet in pay:
                    print("true")
                    print(wallet + ": " + str(pay[wallet]))
                    sql = "INSERT INTO giveaway_payout_history (Transaction, Wallet_Address, Amount, Tickets, giveaway_ID) VALUES (%s, %s, %s,(SELECT lottery_tickets+referral_tickets FROM giveaway_tickets WHERE b3_wallet = %s ORDER BY giveaway_id DESC LIMIT 1), (SELECT MAX(ID) FROM daily_giveaway))"
                    await commit_mysql_query(sql, tx, wallet, pay[wallet], wallet)
    sql = "UPDATE daily_giveaway SET paid=1, total_tickets=%s ORDER BY ID DESC LIMIT 1"
    await commit_mysql_query(sql, total_tickets)


async def get_payment_format(payouts):
    payment_format = {}
    payments_to_make = copy.deepcopy(payouts)
    for win in payments_to_make:
        key, value = win.popitem()
        if key in payment_format:
            payment_format[key] += value
        else:
            payment_format[key] = value
    return payment_format


async def make_giveaway_payment():
    print("Making giveaway payments")
    giveaway = await get_giveaway()
    print("Giveaway = " + str(giveaway))
    giveaway_tickets = await get_giveaway_tickets(giveaway['start'], giveaway['end'])
    total_giveaway_tickets = len(giveaway_tickets)
    print("Giveaway tickets list = " + str(len(giveaway_tickets)))
    winners = await get_random_list_length(10, giveaway_tickets)
    print("winners = " + str(winners))
    payouts = await get_giveaway_win_amounts(winners, giveaway['amount'])
    print('Payouts = ' + str(payouts))
    print('Making payments')
    payment_format = await get_payment_format(payouts)
    transactions = await make_payments(payment_format)
    print("Recording transactions")
    await record_giveaway_transactions(transactions, payouts, total_giveaway_tickets)


async def handle_giveaway():
    sql = "SELECT * FROM daily_giveaway WHERE end = DATE_ADD(UTC_DATE(), INTERVAL 1 DAY)"
    res = await get_mysql_query(sql)
    if len(res) > 0:
        print("Giveaway still running")
        return
    else:
        print("Giveaway ended!")
        await make_giveaway_payment()


async def update_player_giveaway_tickets(tickets):
    print("Updating player giveaway tickets")
    for wallet in tickets:
        wallet_address = wallet[0]
        ticket_count = wallet[1]
        sql = "INSERT INTO giveaway_tickets (b3_wallet, giveaway_id, lottery_tickets) VALUES (%s, (SELECT ID FROM daily_giveaway ORDER BY ID DESC LIMIT 1),%s) ON DUPLICATE KEY UPDATE lottery_tickets=lottery_tickets+%s"
        await commit_mysql_query(sql, wallet_address, ticket_count, ticket_count)
        sql = "SELECT b3_wallet FROM referrals INNER JOIN wallet_addresses ON referrals.Link = wallet_addresses.Link WHERE Wallet_Address=%s"
        res = await get_mysql_query_arg(sql, wallet_address)
        if len(res) > 0:
            for row in res:
                ref_wallet = row[0]
                sql = "INSERT INTO giveaway_tickets (b3_wallet, giveaway_id, referral_tickets) VALUES (%s, (SELECT ID FROM daily_giveaway ORDER BY ID DESC LIMIT 1), %s) ON DUPLICATE KEY UPDATE referral_tickets=referral_tickets+%s"
                await commit_mysql_query(sql, ref_wallet, ticket_count, ticket_count)


async def record_faucet_transactions(transactions, payments):
    for tx in transactions:
        for wallet in transactions[tx]:
            sql = "INSERT INTO faucet_payment_history (Wallet_Address, Amount, Transaction_ID) VALUES (%s, %s, %s)"
            await commit_mysql_query(sql, wallet, round_down(payments[wallet]), tx)
            sql = "DELETE FROM faucet_pending where wallet_address=%s"
            await commit_mysql_query(sql, wallet)


async def pay_faucet():
    payments = {}
    sql = "SELECT * from faucet_pending"
    res = await get_mysql_query(sql)
    print(res)
    if len(res) > 0:
        for row in res:
            payments[row[0]] = float(row[1])
        print("payments = " + str(payments))
        print("Getting faucet transactions")
        transactions = await make_payments(payments)
        print(transactions)
        print("recording transactions")
        await record_faucet_transactions(transactions, payments)


async def update_faucet_balance(remaining_funds):
    sql = "UPDATE faucet_balance SET available=available+%s*0.20 ORDER BY date DESC LIMIT 1"
    await commit_mysql_query(sql, remaining_funds)


async def get_auto_players():
    print("Getting automated players")
    sql = "SELECT b3_wallet from wallet_addresses WHERE auto_play=1"
    results = await get_mysql_query_arg(sql)
    addresses = [];
    for row in results:
        addresses.append(row[0]);
    print(results)
    print(addresses)
    return addresses


async def record_auto_payments(payments, node_earnings, tickets):
    print("node earnings" + str(node_earnings))
    for wallet in tickets:
        wallet_address = wallet[0]
        ticket_count = wallet[1]
        win = 0
        if wallet_address in payments:
            win = payments[wallet_address]
        lose = 0
        if wallet_address in node_earnings:
            lose = node_earnings[wallet_address]
        sql = "INSERT INTO history_player (Round_ID, Wallet_Address, Tickets, Win_Total, Lose_Total, Paid_Transaction_ID) VALUES ((SELECT MAX(ID) FROM game_round), %s, %s, %s, %s, 'Auto Play')"
        await commit_mysql_query(sql, wallet_address, ticket_count, round_down(win), round_down(lose))


async def handle_auto_players(payments, auto_players, nodeearnings, tickets):
    global auto_player_balance
    print("handling auto players")
    dict = auto_player_balance.copy()
    auto_copy = auto_players.copy()
    print(dict)
    #    check for players who turned auto off and add to payments
    print('Checking for old auto accounts')
    for player in dict:
        if player not in auto_players:
            print("Player not in auto players")
            if player in payments:
                payments[player] = payments[player] + auto_player_balance[player]
                print('old auto found adding rest of balance to payoute')
            else:
                payments[player] = auto_player_balance[player]
                print('old auto found creating payout')
            sql = "DELETE FROM auto_balances WHERE wallet=%s"
            await commit_mysql_query(sql, player)
            print("removing player from auto player bal list")
            auto_player_balance.pop(player)

    print('Checking currently running auto accounts')
    for player in auto_copy:
        if player in payments:
            print("player in payments")
            bal = payments[player]  ##1470
            total = bal
            print('bal = ' + str(bal))
            ##adds remaining bal to winnings
            if player in auto_player_balance:
                print("player in auto_player_balance")
                print("old bal - " + str(auto_player_balance[player]))
                total += auto_player_balance[player]
            if total >= 10:
                print(" > 10")
                print('auto player -' + player + ' - balance ' + str(total) + ' old bal ' + str(total - bal))
                auto_player_balance[player] = total
                sql = "INSERT INTO auto_balances (wallet, balance) VALUES (%s,%s) ON DUPLICATE KEY UPDATE balance=%s"
                await commit_mysql_query(sql, player, total, total)
            else:
                print("Total < 10")
                payments[player] = payments[player] + auto_player_balance[player]
                print('old auto found adding rest of balance to payoute')
                sql = "UPDATE wallet_addresses SET auto_play = 0 WHERE b3_wallet=%s"
                await commit_mysql_query(sql, player)
                sql = "DELETE FROM auto_balances WHERE wallet=%s"
                await commit_mysql_query(sql, player)
                if player in auto_player_balance:
                    print('removing from auto_balances')
                    auto_player_balance.pop(player)
                print('removing from auto_player')
                auto_players.pop(player)
    print("New Auto players " + str(auto_player_balance))


async def get_auto_ticket_count():
    global auto_player_balance
    print("Getting auto player tickets count")
    print(auto_player_balance)
    total = 0
    for player in auto_player_balance:
        tickets = (auto_player_balance[player] - (auto_player_balance[player] % 10)) / 10
        total += tickets
    return total

    ##TODO leave in payments and remove at payment area


async def add_auto_tickets(tickets):  ##(('SVuB5BJymcSSaFgzrAmazqhaf1WLUYnbGg', Decimal('10')),) int(Decimal('10))
    global auto_player_balance
    auto_players = dict(auto_player_balance)
    print("ADDING auto player tickets")
    for data in tickets:
        wallet = data[0]
        amount = int(data[1])
        if wallet in auto_player_balance:
            print("player already has tickets in ticket list adding more " + str(amount))
            auto_ticks = int((auto_player_balance[wallet] - (auto_player_balance[wallet] % 10)) / 10)
            print('data = ' + str(data))
            print('auto = ' + str(auto_ticks))
            print("data1 = " + str(data[1]))
            data[1] = int(int(amount) + auto_ticks)
            print("player already has tickets in ticket list adding more  new " + str(data[1]))
            print('autoplayer bal =' + str(auto_player_balance[wallet]))
            print('updated player bal - tick cost =' + str((auto_ticks * 10)) + ' ' + str(
                auto_player_balance[wallet] - (auto_ticks * 10)))
            auto_player_balance[wallet] = auto_player_balance[wallet] - (auto_ticks * 10)
            auto_players.pop(wallet)
    for player in auto_players:
        print("Player inside auto")
        auto_ticks = int((auto_player_balance[player] - (auto_player_balance[player] % 10)) / 10)
        print('Auto ticks = ' + str(auto_ticks))
        tickets.append([player, auto_ticks])
        print('Player auto bal = ' + str(auto_player_balance[player]))
        auto_player_balance[player] = auto_player_balance[player] - (auto_ticks * 10)
        print('Player auto bal = ' + str(auto_player_balance[player]))
    print("Inside - " + str(tickets))
    return tickets


async def finalise_current_round():
    global game_finishing
    print('Getting Tickets')
    tickets = await get_player_tickets()
    print("Tickets 1 " + str(tickets))
    tickets = await add_auto_tickets(tickets)
    print("Tickets 2 " + str(tickets))
    tickets_list = await get_tickets_list(tickets)  # array of addresses [ad1, add1, add1, ad2]
    total_pool = len(tickets_list) * default_ticket_cost
    print("Total Pool = " + str(total_pool))
    print('Tickets list = ' + str(len(tickets_list)))
    random_list = await get_random_list(tickets_list)  # randomed array of addresses
    group_sizes = await get_group_sizes(random_list)  # g1 1 g2 2 g3 3 g4 2 g5 1
    print("Calculating winners")
    winners_pool = await get_winners_pool(group_sizes)  # 90,000
    print('Winners Pool = ' + str(winners_pool))
    losers_pool = total_pool - winners_pool  # 10,000
    print('Losers Pool = ' + str(losers_pool))
    payout_amounts = await get_payout_amounts(group_sizes, losers_pool)  # g1 13500, g2 11000, g3 10500
    remaining_funds = await get_remaining_funds(total_pool)  # 200
    print("Remaining funds = " + str(remaining_funds))
    payments = await calculate_payments(random_list, group_sizes, payout_amounts)  # {wallet:amount won}
    print("Final payments = " + str(payments))
    loser_count = len(random_list)
    print("Losers count = " + str(loser_count))
    print('Adding node earners')
    node_earnings = await calculate_node_earnings(remaining_funds, random_list)
    print('Making Payments')
    auto_players = await get_auto_players();
    print(auto_players)
    ##leave in paymentys
    await handle_auto_players(payments, auto_players, node_earnings, tickets)
    print('payment length = ' + str(len(payments)))
    ##check against auto list and dont send tx. set txid as AUTO
    transactions = await make_payments(payments, True)
    print('Recording Payments')
    await record_transaction(payments, transactions)
    await update_player_stats(tickets, payments, transactions, node_earnings)
    print('Recording node earnings')
    await record_node_earnings(node_earnings)
    await update_player_giveaway_tickets(tickets)
    print("Dealing with faucet payments")
    await pay_faucet()
    print("updating faucet balance")
    await update_faucet_balance(remaining_funds)
    print('Updating giveaway with remaining funds ' + str(remaining_funds))
    await update_giveaway(remaining_funds)
    print("Check if giveaway has ended")
    await handle_giveaway()
    game_finishing = False


async def start_game(timer):
    global current_timer
    global game_finishing
    global block_completed
    current_timer = timer
    print("Starting new game " + str(timer) + " seconds long")
    auto_tickets = await get_auto_ticket_count()
    if auto_tickets > 0:
        sql = "UPDATE game_round set Tickets_Sold=%s ORDER BY ID DESC LIMIT 1"
        await commit_mysql_query(sql, auto_tickets)
    await add_player(auto_tickets)
    while current_timer > 0:
        current_timer -= 1
        seconds = current_timer % 60
        mins = current_timer / 60 % 60
        hours = current_timer / 3600
        if hours == 0 and mins == 0:
            print("%d:%02d:%02d" % (hours, mins, seconds))
        elif int(seconds) % 30 == 0:
            print("%d:%02d:%02d" % (hours, mins, seconds))
        await asyncio.sleep(1)
        if current_timer <= 0 and current_players < 10:
            await add_time(1800)
    game_finishing = True
    while not block_completed:
        print('Block not completed')
        await asyncio.sleep(10)
    print('Block completed')
    await  asyncio.sleep(30)
    await finalise_current_round()
    await check_node_payment()
    await check_game()


async def get_mysql_query(string):
    result = None
    if connection is None or not connection.open:
        await establish_mysql()
    try:
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql)
            result = conn.fetchall()
    except BrokenPipeError:
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql)
            result = conn.fetchall()
    finally:
        connection.close()
        return result


async def get_mysql_query_arg(string, *args):
    result = None
    if connection is None or not connection.open:
        await establish_mysql()
    try:
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql, args)
            result = conn.fetchall()
    except BrokenPipeError:
        establish_mysql()
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql, args)
            result = conn.fetchall()
    finally:
        connection.close()
        if result is None:
            return {}
        return result


async def commit_mysql_query(string, *args):
    if connection is None or not connection.open:
        await establish_mysql()
    try:
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql, args)
        connection.commit()
    except BrokenPipeError:
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql, args)
        connection.commit()
    finally:
        connection.close()


async def open_mysql():
    if connection is None or not connection.open:
        await establish_mysql()


async def close_mysql():
    connection.close()


async def commit_query(string, *args):
    try:
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql, args)
        connection.commit()
    except BrokenPipeError:
        with connection.cursor() as conn:
            sql = string
            conn.execute(sql, args)
        connection.commit()


async def add_time(seconds):
    print('Adding time')
    global current_timer
    current_timer += int(seconds)
    await update_time(seconds)
    return "Added time - " + str(seconds)


async def update_time(seconds):
    global current_end
    date = await get_future_datetime(current_end, seconds)
    formated = await format_datetime(date)
    sql = "UPDATE game_round SET End=%s ORDER BY ID DESC LIMIT 1"
    await commit_mysql_query(sql, formated)
    current_end = date


async def remove_time(seconds):
    global current_timer
    if current_timer > 0:
        if current_timer < 1800:
            return
        timer = current_timer
        secs = int(seconds)
        if timer - secs < 1800:
            current_timer = 1800
            await update_time(-(timer - 1800))
        else:
            current_timer -= secs
            await update_time(-secs)
    return "Removed time - " + str(seconds)


async def add_player(number):
    global current_players
    global current_timer_val
    print("Adding player - " + str(number))
    current_players += int(number)
    print("Current players = " + str(current_players))
    newval = (int(current_players / players_decrease))
    print("timer val new  = " + str(newval))
    if newval != current_timer_val:
        change = abs(newval - current_timer_val)
        print("Change = " + str(change) + " changing by " + str(change * default_decrease))
        await remove_time(change * default_decrease)
        current_timer_val = newval
    return "Added " + str(number) + " tickets, current tickets = " + str(current_players)


async def check_node_payment():
    global node_payment
    global pending_node_payments
    print("Checking for pending node payments")
    print(pending_node_payments)
    for payment in pending_node_payments:
        tx = payment
        result = b3coindnodes.gettransaction(tx)
        print("Node payment processing " + tx)
        rxaddress = None
        rxamount = None
        tx_type = None
        try:
            details = result['details']
            try:
                rxaddress = details[0]['address']
            except IndexError:
                ##does not exist remove
                pending_node_payments.remove(payment);
                sql = "DELETE FROM node_rewards WHERE Transaction=%s"
                await commit_mysql_query(sql, tx)
                continue
            rxamount = int(result['amount'])
            tx_type = details[0]['category']
        except KeyError:
            for out in result['vout']:
                if out['scriptPubKey']['type'] == 'pubkeyhash':
                    rxaddress = out['scriptPubKey']['addresses'][0]
                    rxamount = out['value']
                    if result['confirmations'] < 50:
                        tx_type = 'immature'
                    else:
                        tx_type = 'generate'
        print("address = " + str(rxaddress))
        print("amount = " + str(rxamount))
        print("type = " + str(tx_type))
        if tx_type == 'generate':
            print("Adding node payment to payment queue " + str(rxamount) + " " + str(rxaddress))
            await queue_node_payment(rxamount, tx, rxaddress)
            pending_node_payments.remove(payment)
            sql = "SELECT Wallet_Address, Amount, Node_Waiting_For FROM node_deposit_pending WHERE Node_Waiting_For = (SELECT ID FROM nodes WHERE Node_Wallet = %s)"
            res = await get_mysql_query_arg(sql, rxaddress)
            if len(res) > 0:
                for pending in res:
                    sql = "INSERT INTO node_owners (Wallet_Address, Amount, Direct_Deposit) VALUES (%s, 0, %s) ON DUPLICATE KEY UPDATE Direct_Deposit=Direct_Deposit+%s"
                    await commit_mysql_query(sql, pending[0], pending[1], pending[1])
                    sql = "DELETE FROM node_deposit_pending WHERE Wallet_Address=%s AND Node_Waiting_For=%s"
                    await commit_mysql_query(sql, pending[0], pending[2])
    print("Checking node payment" + str(node_payment))
    if len(node_payment) > 0:
        await make_payment_to_fn_owners()


async def get_node_owners():
    sql = "SELECT node_owners.Wallet_Address, (node_owners.Amount+((IFNULL(node_deposit_pending.Amount,0)*0.25)*0.98)+(Direct_Deposit*0.98))/(SELECT SUM(Amount)+SUM(Direct_Deposit)+(SELECT IFNULL(SUM(Amount)*0.25,0) FROM node_deposit_pending) FROM node_owners) AS total FROM node_owners LEFT JOIN node_deposit_pending ON node_owners.Wallet_Address=node_deposit_pending.Wallet_Address"
    return await get_mysql_query(sql)


async def store_small_payments(storage):
    for wallet in storage:
        sql = "INSERT INTO node_payments_owed (Wallet_Address, Amount) VALUES (%s, %s) ON DUPLICATE KEY UPDATE Amount=%s"
        await commit_mysql_query(sql, wallet, storage[wallet], storage[wallet])


async def get_node_payments(amount, node_owners):  # 0-address 1-amount
    payments = {}
    storage = {}
    print("Getting node paymentns for " + str(amount))
    amount -= ((int(len(node_owners) / 20) + 5) * 0.002)
    print("amount now =  " + str(amount))
    print("Getting owed payments")  # TODO SLOW AS FUCK
    sql = "SELECT * FROM node_payments_owed"
    res = await get_mysql_query(sql)
    stored_payments = {}
    for row in res:
        stored_payments[row[0]] = float(row[1])

    print("Node owner loop")
    for row in node_owners:
        val = round_down(float(row[1]) * amount)
        if row[0] in stored_payments:
            val += stored_payments[row[0]]
            if val > 1.00:
                sql = "DELETE FROM node_payments_owed WHERE Wallet_Address=%s"
                await commit_mysql_query(sql, row[0])
        if val < 1.00:
            storage[row[0]] = val
        else:
            payments[row[0]] = val
    print("storing small payments")
    await store_small_payments(storage)
    return payments


async def record_fn_transactions(payments, transactions, paying_tx):
    for tx in transactions:
        await open_mysql()
        for wallet in transactions[tx]:
            sql = "INSERT INTO node_payouts_history (Transaction, Wallet_Address, Amount, Paying_Tx) VALUES (%s, %s, %s, %s)"
            await commit_query(sql, tx, wallet, payments[wallet], paying_tx)
        await close_mysql()


async def record_fn_payment_data(amount, tx, node_wallet):
    sql = "UPDATE node_rewards SET Paid=TRUE WHERE Transaction=%s"
    await commit_mysql_query(sql, tx)
    sql = "UPDATE nodes SET Total_Earnt=Total_Earnt+%s WHERE Node_Wallet=%s"
    await commit_mysql_query(sql, amount, node_wallet)


async def make_payment_to_fn_owners():
    global node_payment
    # node_payment = {amount, tx, id, full t/f}
    for nodepayment in node_payment:
        sql = "SELECT * FROM node_rewards WHERE Paid=1 AND Transaction = %s"
        res = await get_mysql_query_arg(sql, nodepayment['tx'])
        if len(res) > 0:
            print("Done Already paid removing")
            node_payment.remove(nodepayment)
            print("nodes remaining = " + str(node_payment))
        else:
            amount = nodepayment['amount']
            print("Paying node owners " + str(amount))
            node_owners = await get_node_owners()  # wallet:2.24  row[][0] row[][1]
            payments = await get_node_payments(amount, node_owners)
            print("Sending payments to FN owners")
            transactions = await make_node_payments(payments)
            print("Recording transactions")
            await record_fn_transactions(payments, transactions, nodepayment['tx'])
            print("Recording fn payment data")
            await record_fn_payment_data(nodepayment['amount'], nodepayment['tx'], nodepayment['node_wallet'])
            print("Done paying out node")
            node_payment.remove(nodepayment)
            print("nodes remaining = " + str(node_payment))


async def check_giveaway_running():
    print("checking giveaway status")
    sql = "SELECT * FROM daily_giveaway WHERE end = DATE_ADD(UTC_DATE(), INTERVAL 1 DAY) OR paid=0"
    res = await get_mysql_query(sql)
    if len(res) > 0:
        print("giveaway running!")
        return
    else:
        print("Creating giveaway")
        sql = 'INSERT INTO daily_giveaway (start, end, amount, paid) VALUES (UTC_DATE(), DATE_ADD(UTC_DATE(), INTERVAL 1 DAY), 0, 0)'
        await commit_mysql_query(sql)
        print("updating daily faucet balance")
        sql = "INSERT INTO faucet_balance (date, available) VALUES (NOW(), (SELECT available*0.95 from (SELECT * FROM faucet_balance) as x ORDER BY date desc limit 1))"
        await commit_mysql_query(sql)


async def create_new_game(timer):
    global current_end
    global current_players
    global block_completed
    global current_timer_val
    global round_id
    sql = "INSERT INTO game_round (Start, End, Tickets_Sold, Paid) VALUES (%s, %s, %s, %s)"
    start = await get_datetime()
    end = await get_future_datetime(start, timer)
    await commit_mysql_query(sql, await format_datetime(start), await format_datetime(end), 0, False)
    await check_giveaway_running()
    current_end = end
    current_players = 0
    current_timer_val = 0
    block_completed = False

    sql = "SELECT ID FROM game_round ORDER BY ID DESC LIMIT 1"
    results = await get_mysql_query(sql)
    id = 0
    for row in results:
        val = int(row[0])
        if val > id:
            id = val
    print("Round = " + str(id))
    round_id = id
    await start_game(timer)


async def load_last_game():
    global current_players
    global current_end
    global current_timer_val
    global pending_node_payments
    print("Loading last round")
    global current_timer
    global auto_player_balance
    global round_id

    sql = "SELECT Transaction FROM node_rewards WHERE Paid = 0"
    results = await get_mysql_query(sql)
    for row in results:
        print("Adding node payments")
        if row[0] not in pending_node_payments:
            pending_node_payments.append(row[0])
    print(pending_node_payments)

    sql = "SELECT * from auto_balances"
    results = await get_mysql_query(sql)
    for row in results:
        auto_player_balance[row[0]] = float(row[1])

    sql = "SELECT ID FROM game_round ORDER BY ID DESC LIMIT 1"
    results = await get_mysql_query(sql)
    id = 0
    for row in results:
        val = int(row[0])
        if val > id:
            id = val
    print("Round = " + str(id))
    round_id = id

    print("auto players -" + str(auto_player_balance))
    sql = "SELECT Start, End, Tickets_Sold, Paid FROM game_round ORDER BY ID DESC LIMIT 1"
    results = await get_mysql_query(sql)
    for row in results:
        now = await get_datetime()
        end = row[1]
        tickets = row[2]
        paid = row[3]

        current_players = tickets
        current_timer_val = (int(current_players / 20))
        current_end = end
        seconds = (end - now).total_seconds()
        if seconds > 0 and not paid:
            current_timer = seconds
            print('Loaded running round')
        elif not paid:
            await add_time(default_increase)


async def check_game():
    print('Checking game')
    await load_last_game()
    global current_timer
    if current_timer <= 0:
        await create_new_game(default_timer)
    else:
        await start_game(current_timer)


async def force_timer(seconds):
    global current_timer
    current_timer = int(seconds)
    await update_time(seconds)
    return "Forced timer to " + str(seconds)


async def block_finished():
    global block_completed
    if game_finishing:
        block_completed = True
    else:
        block_completed = False


async def set_node_payment(tx):
    global pending_node_payments
    print("Adding node payment to pending " + tx)
    pending_node_payments.append(tx)
    print('Pending node payments = ' + str(pending_node_payments))


async def queue_node_payment(amount, tx, node_wallet):
    global node_payment
    node_payment.append({'amount': float(amount), 'full': True, 'tx': tx, 'node_wallet': node_wallet})
    print("Set node payment to " + str(node_payment))
    return "Node payment set as " + str(node_payment)


async def handle_data(string):
    print('handliing ' + string)
    while game_finishing and block_completed:
        # print("Game Paused Waiting...")
        await asyncio.sleep(10)
        if string.split()[0] == 'add_player':
            return
    command = string.split()
    if command[0] == 'block_finished':
        print("Block finished")
        await block_finished()
        return "Block command received"
    elif command[0] == 'add_time':
        print('adding time ' + command[1] + "s")
        return await add_time(command[1])
    elif command[0] == 'remove_time':
        print('removing time ' + command[1] + "s")
        return await remove_time(command[1])
    elif command[0] == 'add_player':
        return await add_player(command[1])
    elif command[0] == 'force_timer':
        return await force_timer(command[1])
    elif command[0] == 'get_tickets':
        return "Current Players = " + str(current_players)
    elif command[0] == 'node_full_payment':
        return await set_node_payment(command[1])
    else:
        return "Not a command"


async def handle_connection(reader, writer):
    addr = writer.get_extra_info('peername')
    data = await reader.readline()
    print("Server received {!r} from {}".format(data.decode(), addr))
    result = await handle_data(data.decode())
    print(result)
    # writer.write(result.encode())
    # await writer.drain()
    # writer.close()
    print('Message Sent')


loop = asyncio.get_event_loop()
loop.create_task(check_game())
# sslctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
# sslctx.load_cert_chain(certfile='cert',
#                        keyfile='key')
# coro = asyncio.start_server(handle_connection, '127.0.0.1', 9000, ssl=sslctx, loop=loop)
coro = asyncio.start_server(handle_connection, '127.0.0.1', 9000, loop=loop)
server = loop.run_until_complete(coro)
print('Serving on {}'.format(server.sockets[0].getsockname()))
loop.run_forever()
loop.close()
