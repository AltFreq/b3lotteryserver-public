import os

import pymysql
from slickrpc import Proxy

rpc_address = None
rpc_port = None
rpc_user = None
rpc_pass = None
mysqql_ssl = {'cert': '/usr/lib/b3lotteryserver/client-cert.pem',
              'key': '/usr/lib/b3lotteryserver/client-key.pem'}
connection = None

with open(os.path.expanduser('~/.B3-CoinV2/b3coin.conf')) as file:
    line = file.readline()
    while line:
        data = line.strip()
        if data.startswith("rpcbind"):
            rpc_address = data.split('=')[1]
        elif data.startswith("rpcport"):
            rpc_port = data.split('=')[1]
        elif data.startswith("rpcpassword"):
            rpc_pass = data.split('=', 1)[1]
        elif data.startswith("rpcuser"):
            rpc_user = data.split('=', 1)[1]
        line = file.readline()

b3coind = Proxy("http://%s:%s@%s:%s" % (rpc_user, rpc_pass, rpc_address, rpc_port))


def establish_mysql():
    global connection
    connection = pymysql.connect(host='mysql_server',
                                 user='AltFreq',
                                 passwd='Ms016356!01',
                                 db="b3_lottery",
                                 charset="utf8",
                                 ssl=mysqql_ssl)


def get_mysql_query(string):
    if connection is None or not connection.open:
        establish_mysql()
    try:
        with connection as conn:
            sql = string
            conn.execute(sql)
            conn.close()
            return conn.fetchall()
    except BrokenPipeError:
        with connection as conn:
            sql = string
            conn.execute(sql)
            conn.close()
            return conn.fetchall()


sql = "SELECT generated_wallet FROM wallet_addresses"
res = get_mysql_query(sql)
for row in res:
    print(b3coind.dumpprivkey(row[0]))
